#include <string>
#include <iostream>
#include <thread>

using namespace std;

void foo (string msg) {
    cout << " In foo: " << msg;
}

void bar (string msg) {
    cout << " In bar: " << msg;
}

int main()
{
    // Constructs the new thread and runs it. Does not block execution.
    thread t1(foo, "Hello from Task 1");
    thread t2(bar, "Hello from Task 2");

    // Makes the main thread wait for the new thread to finish execution,
    // blocking the main thread until join returns
    t1.join();
    t2.join();
}
