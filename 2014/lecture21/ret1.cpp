#include <iostream>

struct node
{
	int data;
};

const node & makeNode(node &cref);

int main()
{
	node nodeA = {0};
	std::cout << "1: nodeA.data = " << nodeA.data << std::endl;
	makeNode(nodeA);
	std::cout << "2: nodeA.data = " << nodeA.data << std::endl;
	node nodeB;
	nodeB = makeNode(nodeA);
	std::cout << "3: nodeA.data = " << nodeA.data << std::endl; 
	std::cout << "1: nodeB.data = " << nodeB.data << std::endl;

	node nodeC;
	// makeNode(nodeB).data = 99;

	return 0;
}

const node & makeNode(const node &ref)
{
	std::cout << "call makeNode()\n";
	ref.data++;
	const return ref;
}
