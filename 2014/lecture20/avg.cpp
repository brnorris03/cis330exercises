/*
 * avg.cpp
 *
 *  Created on: Feb 19, 2014
 *      Author: norris
 */

#include <iostream>
#include <fstream>
#include <set>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

using namespace std;
int main() {

	SDL_Init(0);

	ifstream input("data.txt");
	multiset<int> values;
	/* Read the data from the file. */
	int currValue;
	while (input >> currValue)
		values.insert(currValue);
	/* Compute the average. */
	double total = 0.0;
	for (multiset<int>::iterator itr = values.begin();
			itr != values.end(); ++itr)
		total += *itr;
	cout << "Average is: " << total / values.size() << endl;
}

