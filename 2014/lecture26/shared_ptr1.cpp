#include <iostream>
#include <memory>

using namespace std;

class MyClass {
public:
    MyClass(){
        cout << "Creating MyClass object" << endl;
    }
    virtual ~MyClass(){
        cout << "Destroying MyClass object" << endl;
    }
    void method(){
        cout << "Called method of MyClass object" << endl;
    }
};

shared_ptr<MyClass> returnTest() {
    shared_ptr<MyClass> m(new MyClass);
    m->method();
    return m;
}
 
int main() {
    shared_ptr<MyClass> m2 = returnTest();
    m2->method();
    return 0;
}
