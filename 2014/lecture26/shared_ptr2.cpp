// File: shared_ptr2.cpp
#include <iostream>
#include <memory>

using namespace std;

class A {
public:
    void f() {}
};

int main() {
   shared_ptr<A> pA1(new A());
   pA1->f();
   if (pA1) {} 
   A& a = *pA1;

   cout << "pA1 use_count = " << pA1.use_count() << endl;
   shared_ptr<A> pA2(pA1);
   cout << endl << "After creating pA2:" << endl;
   cout << "pA1 use_count = " << pA1.use_count() << endl;
   cout << "pA2 use_count = " << pA2.use_count() << endl;

    pA1.reset();
    cout << endl << "After reset:" << endl;
    cout << "pA1 use_count = " << pA1.use_count() << endl;
    cout << "pA2 use_count = " << pA2.use_count() << endl;
}
