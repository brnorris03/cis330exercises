/* Filename:  except.cpp

   Programmer:  Br. David Carlson

   Date:  June 6, 1999.

   Last Revised:  December 23, 2001

   This program's only purpose is to illustrate C++ exception handling.
   It prints a sequence of integers to the screen, stopping (by
   "throwing" an exception) when the current integer gets to a
   prearranged stopping value.

   Tested with:
      Microsoft Visual C++ 6.0
      Microsoft Visual C++ .NET
      g++ under Linux
*/

#include <iostream>
using namespace std;


/* Given:  StopNum   The stopping number to be used.
   Task:   To print successive integers 1, 2, 3, ... StopNum - 1.
           Throws an integer exception when StopNum is reached.
   Return: Nothing.
*/
void PrintSequence(int StopNum) {
   int Num;

   Num = 1;
   while (true) {
      if (Num >= StopNum)
         throw Num;
      cout << Num << endl;
      Num++;
   }
}

int main(void) {
   try {
      PrintSequence(20);
   } catch(int ExNum) {
      cout << "Caught an exception with value: " << ExNum << endl;
   }

   return 0;
}


