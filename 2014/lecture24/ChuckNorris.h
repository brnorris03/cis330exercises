
#define MAXTIMES 10
static int counter=1;
static char *jokes[10] = 
	{"Death once had a \"Near Chuck Norris Experience.\"",
	"All arrays Chuck Norris declares are of infinite size, because Chuck Norris knows no bounds.",
	"Chuck Norris writes code that optimizes itself (out of fear).",
	"There is no 'ctrl' button on Chuck Norris's computer. Chuck Norris is always in control.",
	"Chuck Norris can divide by zero.",
	"Chuck Norris’s first program was 'kill -9'.",
	"Chuck Norris can overflow your stack just by looking at it.",
	"When Chuck Norris throws exceptions, it’s across the room.",
	"Chuck Norris solves NP-complete problems in constant time.",
	"Chuck Norris doesn’t need an OS."};

