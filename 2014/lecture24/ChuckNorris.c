#include <stdlib.h> 
#include <stdio.h> 
#include <signal.h> 
#include <stdbool.h>

#include "ChuckNorris.h"

void sigint_handler(int sig) { 
  if (counter >= MAXTIMES) exit(0); 
  printf("\n%s\n", jokes[counter++]); 
} 
 
int main(void) { 
  signal(SIGINT, sigint_handler);
  while (true) { 
  } 
} 
