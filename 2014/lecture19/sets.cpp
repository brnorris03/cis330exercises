#include <set>
#include <iostream>
#include <algorithm>    // for_each

using namespace std;

int main() {
	 set<int> s;

	 for(int i = 1; i <= 100; i++) {
	      s.insert(i); // Insert 100 elements, [1..100]
	 }

	 s.insert(42); // does nothing, 42 already exists in set

	 int sum = 0;
	 for (auto it = s.find(31); it != s.end(); it++) {
		 sum += *it;
	 }

	 for(int i = 2; i <= 100; i += 2) {
	      s.erase(i); // Erase even values
	 }

	 cout << "Set size: " << s.size() << endl; // n will be 50

	 // We can avoid hard-coding the size
	 for_each(s.begin(), s.end(),
			 [&s](int i)->void { if (i % 2 != 0) s.erase(i); });

	 cout << "Set size: " << s.size() << endl;
}
